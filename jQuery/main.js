$(function () {
    "use strict";

    var rows = 0;
    var data = [];
    var tbody = $('tbody');

    /** Formatting fns **/
    function formatMoney(val) {
        return "$" + val;
    }

    /** Data->View fns **/
    function addCategoryNamesToSelect() {
        var sampleCats = sampleProductCategories || [];
        var optionsAsString = "<option>Select...</option>";

        for (var i = 0; i < sampleCats.length; i++) {
            optionsAsString += "<option value=''>" + sampleCats[i].name + "</option>";
        }
        $('select#category' + rows).html($(optionsAsString));
    }

    function addCategoryValuesToSelect(catName, row) {
        var sampleCats = sampleProductCategories || [];
        var optionsAsString = "<option>Select...</option>";

        for (var i = 0; i < sampleCats.length; i++) {
            if (sampleCats[i].name === catName) {
                for (var j = 0; j < sampleCats[i].products.length; j++) {
                    optionsAsString += "<option value=''>" + sampleCats[i].products[j].name + "</option>";
                }
            }
        }
        $('select#product' + row).html($(optionsAsString));
    }

    function displayProductPrice(catName, prodName, row) {
        var sampleCats = sampleProductCategories || [];

        if (catName == null || prodName == null) {
            $("span#price" + row).text("");
            return;
        }

        for (var i = 0; i < sampleCats.length; i++) {
            if (sampleCats[i].name === catName) {
                for (var j = 0; j < sampleCats[i].products.length; j++) {
                    if (sampleCats[i].products[j].name === prodName) {
                        data[row].price = sampleCats[i].products[j].price;
                        $("span#price" + row).text(formatMoney(sampleCats[i].products[j].price));
                        return;
                    }
                }
            }
        }
    }

    /** Row manipulation **/
    function addRow() {
        var rowString = '<tr id="' + rows + '"><td><select class="category" id="category' + rows + '"></select></td>' +
            '<td><select class="product" id="product' + rows + '"></select></td>' +
            '<td class="price"><span id="price' + rows + '"></span></td>' +
            '<td class="quantity"><input id="quantity' + rows + '" /></td>' +
            '<td class="price"><span id="subtotal' + rows + '"> </span></td><td><a href="#" class="remove">Remove</a></td></tr>';
        tbody.append(rowString);
        addCategoryNamesToSelect();
        rows++;
    }

    function removeRow(id) {
        $("tr#" + id).remove();
        data.splice(id ,1)
    }

    /*** Subtotal fns ***/
    function recalculateSubtotal(r) {
        return r.price * r.quantity;
    }

    function updateSubtotalView(r, subtot) {
        data[r].subtotal = subtot;
        $("span#subtotal" + r).text(formatMoney(subtot));
    }

    /*** Total fns ***/
    function recalculateTotal() {
        var total = 0;
        for (var i = 0; i < data.length; i++) {
            var subtot = recalculateSubtotal(data[i]);
            updateSubtotalView(i, subtot);
            total += subtot;
        }
        return formatMoney(total);
    }

    /*** Misc ***/
    function getID(el) {
        return $(el).parents("tr").attr("id");
    }

    /** Event fns **/
    $("button#addLine").click(function () {
        addRow()
    });

    $("button#submit").click(function () {
        console.log(JSON.stringify(data));
    });

    tbody.on('click', 'a.remove',function () {
        removeRow(getID(this));
        $("#grandTotal").text(recalculateTotal());
    });

    tbody.on('change', 'select.category', function (d) {
        var row = getID(this);
        var selected = $(this).find(":selected").text();

        var obj = {
            category: selected,
            product: "",
            price: "",
            quantity: 1,
            subtotal: ""
        };

        if (data.length < row) {
            data.push(obj);
        } else {
            data[row] = obj;
        }

        $("input#quantity" + row).hide();
        addCategoryValuesToSelect(selected, row);
        displayProductPrice(null, null, row);
    });

    tbody.on('change', 'select.product', function (d) {
        var row = getID(this);
        var catName = data[row].category;
        var selected = $(this).find(":selected").text();
        data[row].product = selected;
        $("input#quantity" + row).val(1).show();
        displayProductPrice(catName, selected, row);
    });

    tbody.on('change', 'select', function (d) {
        $("#grandTotal").text(recalculateTotal());
    });

    tbody.on('keyup', 'input', function (d) {
        var row = getID(this);
        data[row].quantity = $(this).val();
        $("#grandTotal").text(recalculateTotal());
    });

    /** Init app **/
    addRow();
});
"use strict";

function formatCurrency(value) {
    return "$" + value.toFixed(2);
}

var CartLine = function () {
    var self = this;
    self.category = ko.observable();
    self.product = ko.observable();
    self.quantity = ko.observable(1);
    self.subtotal = ko.computed(function () {
        return self.product() ? self.product().price * parseInt("0" + self.quantity(), 10) : 0;
    });

    // Whenever the category changes, reset the product selection
    self.category.subscribe(function () {
        self.product(undefined);
    });
};

var Cart = function () {
    // Stores an array of lines, and from these, can work out the grandTotal
    var self = this;
    self.lines = ko.observableArray([new CartLine()]); // Put one line in by default
    self.grandTotal = ko.computed(function () {
        var total = 0, lines = self.lines();
        for (var i = 0; i < lines.length; i++) {
            total += lines[i].subtotal();
        }
        return total;
    });

    // Operations
    self.addLine = function () {
        self.lines.push(new CartLine())
    };
    self.removeLine = function (line) {
        self.lines.remove(line)
    };
    self.save = function () {
        var lines = self.lines(), objs = [];
        for (var i = 0; i < lines.length; i++) {
            if (lines[i].product()) {
                objs.push({
                    productName: lines[i].product().name,
                    quantity: lines[i].quantity()
                })
            }
        }
        console.log(JSON.stringify(objs));
    };
};

ko.applyBindings(new Cart());